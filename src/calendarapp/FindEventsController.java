package calendarapp;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class FindEventsController implements Initializable {

    @FXML
    private Button btnClose, btnMinimize, btnSearch;
    @FXML
    private TextFlow tfResultSearch;
    @FXML
    private TextField tfSearch;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnClose.setText("\uf057");
        btnMinimize.setText("\uf0ab");
    }

    @FXML
    private void appExit(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void appMinimize(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void findeventAction(ActionEvent event) throws SQLException {
        Text txtResult = new Text("");
        tfResultSearch.getChildren().clear();
        Boolean b1res, b2res;
        if (tfSearch.getLength() > 0) {
            Connection connect = null;
            Statement stmt = null;
            try {
                Class.forName("org.sqlite.JDBC");
                connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
                stmt = connect.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS where DESCRIPTION like '%" + tfSearch.getText() + "%' ;");
                if (!rs.isBeforeFirst()) {
                    b1res = false;
                } else {
                    b1res = true;
                    while (rs.next()) {
                        if (rs.getString("Month").equals("0")) {
                            if (rs.getInt("Important") == 1) {
                                txtResult = new Text("\u26AB " + rs.getString("DAY") + " числа каждого месяца: " + rs.getString("DESCRIPTION") + "\n");
                                txtResult.setFill(Color.RED);                                
                                tfResultSearch.getChildren().add(txtResult);
                            } else {
                                txtResult = new Text("\u26AB " + rs.getString("DAY") + " числа каждого месяца: " + rs.getString("DESCRIPTION") + "\n");
                                tfResultSearch.getChildren().add(txtResult);
                            }

                        } else {
                            if (rs.getInt("Important") == 1) {
                                txtResult = new Text(ConvertDate("\u26AB " + rs.getString("DAY"), rs.getString("Month")) + " : " + rs.getString("DESCRIPTION") + "\n");
                                txtResult.setFill(Color.RED);                                
                                tfResultSearch.getChildren().add(txtResult);
                            } else {
                            txtResult = new Text(ConvertDate("\u26AB " + rs.getString("DAY"), rs.getString("Month")) + " : " + rs.getString("DESCRIPTION") + "\n");
                            tfResultSearch.getChildren().add(txtResult);
                            }
                        }
                    }
                }
                rs = stmt.executeQuery("SELECT * FROM weekly_events where DESCRIPTION like '%" + tfSearch.getText() + "%';");
                if (!rs.isBeforeFirst()) {
                    b2res = false;
                } else {
                    b2res = true;
                while (rs.next()) {
                        if (rs.getInt("Important") == 1) {
                                txtResult = new Text("\u26AB " + convertDayofWeek(rs.getString("DAY_OF_WEEK")) + ": " + rs.getString("DESCRIPTION") + "\n");
                                txtResult.setFill(Color.RED);                                
                                tfResultSearch.getChildren().add(txtResult);
                            } else {
                        txtResult = new Text("\u26AB " + convertDayofWeek(rs.getString("DAY_OF_WEEK")) + ": " + rs.getString("DESCRIPTION") + "\n");
                        tfResultSearch.getChildren().add(txtResult);
                        }
                }
                }
                if (!b1res&!b2res){
                    txtResult = new Text("Поиск по вашему запросу не дал результатов. Попробуйте изменить запрос.");
                    txtResult.setFill(Color.RED);
                    //txtResult.setFont(Font.font("Margot", FontWeight.BOLD, 13));
                    txtResult.getStyleClass().add("txt_event_big");
                    tfResultSearch.getChildren().add(txtResult);
                }
                rs.close();
                stmt.close();
                connect.close();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            } finally {
                if (connect != null) {
                    connect.close();
                }
            }
        } else {
            javafx.scene.Node source = (javafx.scene.Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            showPopupMessage("Введите слово для поиска!", stage);
        }
    }

    private String convertDayofWeek(String day) {
        String sOut = day;
        switch (day) {
            case "1":
                sOut = "Каждый понедельник";
                break;
            case "2":
                sOut = "Каждый вторник";
                break;
            case "3":
                sOut = "Каждую среду";
                break;
            case "4":
                sOut = "Каждый четверг";
                break;
            case "5":
                sOut = "Каждую пятницу";
                break;
            case "6":
                sOut = "Каждую субботу";
                break;
            case "7":
                sOut = "Каждое воскресенье";
                break;
        }
        return sOut;
    }

    private String ConvertDate(String sDay, String sMonth) {
        String sOut = sDay + "." + sMonth;
        switch (sMonth) {
            case "1":
                sOut = sDay + " января";
                break;
            case "2":
                sOut = sDay + " февраля";
                break;
            case "3":
                sOut = sDay + " марта";
                break;
            case "4":
                sOut = sDay + " апреля";
                break;
            case "5":
                sOut = sDay + " мая";
                break;
            case "6":
                sOut = sDay + " июня";
                break;
            case "7":
                sOut = sDay + " июля";
                break;
            case "8":
                sOut = sDay + " августа";
                break;
            case "9":
                sOut = sDay + " сентября";
                break;
            case "10":
                sOut = sDay + " октября";
                break;
            case "11":
                sOut = sDay + " ноября";
                break;
            case "12":
                sOut = sDay + " декабря";
        }
        return sOut;
    }

    public static Popup createPopup(final String message) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popup.hide();
            }
        });
        label.getStylesheets().add(CalendarApp.class.getResource("styleapp.css").toExternalForm());
        label.getStyleClass().add("popup");
        popup.getContent().add(label);
        return popup;
    }

    public static void showPopupMessage(final String message, final Stage stage) {
        final Popup popup = createPopup(message);
        popup.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) {
                popup.setX(stage.getX() + stage.getWidth() / 2 - popup.getWidth() / 2);
                popup.setY(stage.getY() + stage.getHeight() / 2 - popup.getHeight() / 2);
            }
        });
        popup.show(stage);
    }
}

package calendarapp;

import java.io.IOException;
import javafx.fxml.Initializable;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javafx.animation.FadeTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.StageStyle;


public class MainMenuController implements Initializable {

    @FXML
    private Button btnConfig, btnSearch, btnNext, btnPrevious, btnClose, btnMinimize;
    @FXML
    private Label lblCurrentDay, lblYesterday, lblTomorrow;
    @FXML
    private TextFlow tfEventToday, tfEventYesterday, tfEventTomorrow;

    Calendar calend;//календарь на текущую дату
    Integer iDateRoll = 0; //точка отсчета для календаря

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadButton();
        try {
            ParseEvents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //загружаем кнопки на панели 
    private void loadButton() {
        btnConfig.setText("\uf271");
        btnSearch.setText("\uf00e");
        btnNext.setText("\uf054");
        btnPrevious.setText("\uf053");
        btnClose.setText("\uf057");
        btnMinimize.setText("\uf0ab");
    }

    @FXML
    private void appExit(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void appMinimize(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    private void btnNext(ActionEvent event) {
        iDateRoll++;
        try {
            ParseEvents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnPrevious(ActionEvent event) {
        iDateRoll--;
        try {
            ParseEvents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //метод преобразующий даты в заданный формат
    private String datetimeinit(String formatDate) {
        DateFormat dateFormat = new SimpleDateFormat(formatDate);
        Date date = new Date();
        return dateFormat.format(date);
    }

    // метод осуществляющий выборку данных из БД по дате
    private void SQLtoListView(String day, String month, String year, TextFlow tf, Boolean FontSize) throws SQLException {
        Text txtEventDay = new Text();
        Connection connect = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            stmt = connect.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS where DAY = '" + day + "' AND MONTH = '" + month + "';");
            while (rs.next()) {
                if (rs.getInt("Important") == 1) {
                    txtEventDay = new Text("\u26AB " + rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.RED);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                } else {
                    txtEventDay = new Text("\u26AB ");
                    txtEventDay.setFill(Color.rgb(0, 102, 128));                                        
                    tf.getChildren().add(txtEventDay);
                    txtEventDay = new Text(rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.BLACK);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                    tfFadeTransition(tf);
                }
            }
            rs = stmt.executeQuery("SELECT * FROM EVENTS where DAY = '" + day + "' AND MONTH = '0';");
            while (rs.next()) {
                if (rs.getInt("Important") == 1) {
                    txtEventDay = new Text("\u26AB " + rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.RED);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                } else {
                    txtEventDay = new Text("\u26AB ");
                    txtEventDay.setFill(Color.rgb(0, 102, 128));                    
                    tf.getChildren().add(txtEventDay);
                    txtEventDay = new Text("Ежемесячное событие: " + rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.DARKGRAY);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                }
            }
            rs = stmt.executeQuery("SELECT * FROM weekly_events where DAY_OF_WEEK = '" + DayOfWeek(day, month, year) + "';");
            while (rs.next()) {
                if (rs.getInt("Important") == 1) {
                    txtEventDay = new Text("\u26AB " + rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.RED);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                } else {
                    txtEventDay = new Text("\u26AB ");
                    txtEventDay.setFill(Color.rgb(0, 102, 128));                                                    
                    tf.getChildren().add(txtEventDay);
                    txtEventDay = new Text("Еженедельное событие: " + rs.getString("DESCRIPTION") + "\n");
                    txtEventDay.setFill(Color.DARKBLUE);                    
                    if (FontSize) {
                        txtEventDay.getStyleClass().add("txt_event_big");
                    } else {
                        txtEventDay.getStyleClass().add("txt_event_small");
                    }
                    tf.getChildren().add(txtEventDay);
                }
            }
            rs.close();
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    //определение дня недели по заданной дате
    private String DayOfWeek(String day, String month, String year) {
        Calendar calendar = Calendar.getInstance();
        //   янв=0,фев=1,март=2...
        calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(day));
        // воскресенье = 1, понедельник =2 ...
        if (calendar.get(Calendar.DAY_OF_WEEK) == 1) {
            return "7";
        } else {
            return String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1);
        }
    }

    // заполняет форму данными из БД
    private void ParseEvents() throws SQLException {
        calend = new GregorianCalendar();
        tfEventToday.getChildren().clear();
        tfEventTomorrow.getChildren().clear();
        tfEventYesterday.getChildren().clear();

        calend.add(Calendar.DAY_OF_YEAR, +iDateRoll);
        String[] arrDate = getDate(calend);
        
        lblCurrentDay.setText(ConvertDate(arrDate[0], arrDate[1]));
        SQLtoListView(arrDate[0], arrDate[1], arrDate[2], tfEventToday, true);

        calend = new GregorianCalendar();
        calend.add(Calendar.DAY_OF_YEAR, -1 + iDateRoll);
        arrDate = getDate(calend);
        lblYesterday.setText(ConvertDate(arrDate[0], arrDate[1]));
        SQLtoListView(arrDate[0], arrDate[1], arrDate[2], tfEventYesterday, false);

        calend = new GregorianCalendar();
        calend.add(Calendar.DAY_OF_YEAR, +1 + iDateRoll);
        arrDate = getDate(calend);
        lblTomorrow.setText(ConvertDate(arrDate[0], arrDate[1]));
        SQLtoListView(arrDate[0], arrDate[1], arrDate[2], tfEventTomorrow, false);
    }

    private String[] getDate(Calendar calend) {
        String sDay = String.valueOf(calend.get(Calendar.DAY_OF_MONTH));
        String sMonth = String.valueOf(calend.get(Calendar.MONTH) + 1);
        String sYear = String.valueOf(calend.get(Calendar.YEAR));
        String[] arrStr = {sDay, sMonth, sYear};
        return arrStr;
    }

    public TextFlow tfFadeTransition(TextFlow tf) {
        FadeTransition fade = new FadeTransition(Duration.millis(500), tf);
        fade.setFromValue(0.1);
        fade.setToValue(1.0);
        fade.setCycleCount(1);
        fade.setAutoReverse(false);
        fade.play();
        return tf;
    }

//метод преобразующий данные в строку с датой
    private String ConvertDate(String sDay, String sMonth) {
        String sOut = sDay + "." + sMonth;
        switch (sMonth) {
            case "1":
                sOut = sDay + " января";
                break;
            case "2":
                sOut = sDay + " февраля";
                break;
            case "3":
                sOut = sDay + " марта";
                break;
            case "4":
                sOut = sDay + " апреля";
                break;
            case "5":
                sOut = sDay + " мая";
                break;
            case "6":
                sOut = sDay + " июня";
                break;
            case "7":
                sOut = sDay + " июля";
                break;
            case "8":
                sOut = sDay + " августа";
                break;
            case "9":
                sOut = sDay + " сентября";
                break;
            case "10":
                sOut = sDay + " октября";
                break;
            case "11":
                sOut = sDay + " ноября";
                break;
            case "12":
                sOut = sDay + " декабря";
        }
        return sOut;
    }

    //обработчик события кнопки "поиск события"
    @FXML
    private void btn_findAction(ActionEvent event) {
        try {
            Stage secondStage = new Stage(StageStyle.TRANSPARENT);
            Parent root = FXMLLoader.load(getClass().getResource("FindEvents.fxml"));
            Scene secondScene = new Scene(root);
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondScene.setFill(null);
            secondStage.setTitle("Поиск событий");
            secondStage.setScene(secondScene);
            secondStage.getIcons().add(new Image(getClass().getResourceAsStream("UIelements/appicon.png")));
            secondStage.show();
            moveScene(secondScene, secondStage);
        } catch (IOException ex) {

        }
    }

    //обработчик события кнопки "редактирование события"
    @FXML
    private void btn_editAction(ActionEvent event) {
        try {
            Stage secondStage = new Stage(StageStyle.TRANSPARENT);
            Parent root = FXMLLoader.load(getClass().getResource("EditDate.fxml"));
            Scene secondScene = new Scene(root);
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondScene.setFill(null);
            secondStage.setTitle("Редактирование событий");
            secondStage.setScene(secondScene);
            secondStage.getIcons().add(new Image(getClass().getResourceAsStream("UIelements/appicon.png")));
            moveScene(secondScene, secondStage);
            secondStage.showAndWait();
            try {
                ParseEvents();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (IOException ex) {
        }
    }

    public void moveScene(Scene scene, Stage stage) {
        final Delta dragDelta = new Delta();
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                dragDelta.x = stage.getX() - mouseEvent.getScreenX();
                dragDelta.y = stage.getY() - mouseEvent.getScreenY();
            }
        });
        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                stage.setX(mouseEvent.getScreenX() + dragDelta.x);
                stage.setY(mouseEvent.getScreenY() + dragDelta.y);
            }
        });
    }

    class Delta {

        double x, y;
    }

}

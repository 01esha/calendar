/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calendarapp;

import com.sun.prism.impl.Disposer.Record;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

public class EditDateController implements Initializable {

    @FXML
    private Button btnClose, btnMinimize;
    @FXML
    private DatePicker datepicker;
    @FXML
    private TableView tblviewEvents;
    @FXML
    private TableColumn tc_id, tc_period, tc_events, tc_imp;
    @FXML
    private TextField tfield_addevent;
    @FXML
    private ChoiceBox cbx_repeat;
    @FXML
    private CheckBox checkboxImp;

    final ObservableList<TableEvent> dataEvents = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnClose.setText("\uf057");
        btnMinimize.setText("\uf0ab");

        tc_id.setCellValueFactory(new PropertyValueFactory<TableEvent, Integer>("ID"));
        tc_period.setCellValueFactory(new PropertyValueFactory<TableEvent, String>("Period"));
        tc_events.setCellValueFactory(new PropertyValueFactory<TableEvent, String>("Events"));
        tc_imp.setCellValueFactory(new PropertyValueFactory<TableEvent, Integer>("Important"));
        tc_events.setCellFactory(TextFieldTableCell.forTableColumn());
        tc_events.setOnEditCommit(
                new EventHandler<CellEditEvent<TableEvent, String>>() {
                    @Override
                    public void handle(CellEditEvent<TableEvent, String> t) {
                        ((TableEvent) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setEvents(t.getNewValue());
                        try {
                            TableEvent teSelect = (TableEvent) t.getTableView().getItems().get(t.getTablePosition().getRow());
                            updateDB(teSelect.getEvents(), teSelect.getID(), teSelect.getPeriod());
                        } catch (SQLException ex) {
                        }
                    }
                }
        );

        Callback<TableColumn, TableCell> stringCellFactory
                = new Callback<TableColumn, TableCell>() {
                    @Override
                    public TableCell call(TableColumn p) {
                        MyStringTableCell cell = new MyStringTableCell();
                        cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new MyEventHandler());
                        return cell;
                    }
                };
        tc_imp.setCellFactory(stringCellFactory);

        tblviewEvents.setItems(dataEvents);
        datepicker.setValue(LocalDate.now());
        try {
            loadEvents();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        datepicker.setOnAction(event -> {
            dataEvents.clear();
            try {
                loadEvents();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

    }
//обработчик изменения ячейки "Важность"
    class MyStringTableCell extends TableCell<Record, String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? null : getString());
            setGraphic(null);
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

    class MyEventHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent t) {
            TableCell c = (TableCell) t.getSource();
            int index = c.getIndex();            
            Integer iID = dataEvents.get(index).getID();
            String sPer = dataEvents.get(index).getPeriod();
            try {
                updateImportant(iID, sPer);
            } catch (SQLException ex) {
            }
            try {
                dataEvents.clear();
                loadEvents();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateImportant(Integer ID, String Per) throws SQLException {
        Connection connect = null;
        Statement stmt = null;
        Integer iImp = 0;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            connect.setAutoCommit(false);
            stmt = connect.createStatement();
            String sql;
            if (Per.equals("Ежегодное") | Per.equals("Ежемесячное")) {
                sql = "SELECT * FROM EVENTS where ID=" + ID + ";";
            } else {
                sql = "SELECT * FROM WEEKLY_EVENTS where ID=" + ID + ";";
            }
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                iImp = rs.getInt("Important");
            }

            if (Per.equals("Ежегодное") | Per.equals("Ежемесячное")) {
                if (iImp == 1) {
                    sql = "UPDATE EVENTS set IMPORTANT = '" + 0 + "'where ID=" + ID + ";";
                } else {
                    sql = "UPDATE EVENTS set IMPORTANT = '" + 1 + "'where ID=" + ID + ";";
                }
            } else {
                if (iImp == 1) {
                    sql = "UPDATE WEEKLY_EVENTS set IMPORTANT = '" + 0 + "'where ID=" + ID + ";";
                } else {
                    sql = "UPDATE WEEKLY_EVENTS set IMPORTANT = '" + 1 + "'where ID=" + ID + ";";
                }
            }

            stmt.executeUpdate(sql);
            connect.commit();
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    //обновление описания события
    private void updateDB(String Desc, Integer ID, String Per) throws SQLException {
        Connection connect = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            connect.setAutoCommit(false);
            stmt = connect.createStatement();
            String sql;
            if (Per.equals("Ежегодное") | Per.equals("Ежемесячное")) {
                sql = "UPDATE EVENTS set DESCRIPTION = '" + Desc + "'where ID=" + ID + ";";
            } else {
                sql = "UPDATE WEEKLY_EVENTS set DESCRIPTION = '" + Desc + "'where ID=" + ID + ";";
            }
            stmt.executeUpdate(sql);
            connect.commit();            
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    @FXML
    private void appExit(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void appMinimize(ActionEvent event) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.setIconified(true);
    }

    
    private void loadEvents() throws SQLException {
        Connection connect = null;
        Statement stmt = null;
        LocalDate date = datepicker.getValue();
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            stmt = connect.createStatement();
            String sDay;
            if (date.toString().substring(8, 9).equals("0")) //извелкаем из даты подстроку с днем 
            {
                sDay = date.toString().substring(9, 10);
            } else {
                sDay = date.toString().substring(8, 10);
            }
            String sMonth;
            if (date.toString().substring(5, 6).equals("0")) //извелкаем из даты подстроку с месяцем
            {
                sMonth = date.toString().substring(6, 7);
            } else {
                sMonth = date.toString().substring(5, 7);
            }
            ResultSet rs = stmt.executeQuery("SELECT * FROM EVENTS where DAY = '" + sDay + "' AND MONTH = '" + sMonth + "';");
            while (rs.next()) {
                dataEvents.add(new TableEvent(rs.getInt("ID"), "Ежегодное",
                        rs.getString("DESCRIPTION"), String.valueOf(rs.getInt("Important"))));
            }
            rs = stmt.executeQuery("SELECT * FROM EVENTS where DAY = '" + sDay + "' AND MONTH = '0';");
            while (rs.next()) {
                dataEvents.add(new TableEvent(rs.getInt("ID"), "Ежемесячное",
                        rs.getString("DESCRIPTION"), String.valueOf(rs.getInt("Important"))));
            }
            rs = stmt.executeQuery("SELECT * FROM weekly_events where DAY_OF_WEEK = '" + DayOfWeek() + "';");
            while (rs.next()) {
                dataEvents.add(new TableEvent(rs.getInt("ID"), "Еженедельное",
                        rs.getString("DESCRIPTION"), String.valueOf(rs.getInt("Important"))));
            }
            rs.close();
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    @FXML
    private void deleteAction(ActionEvent event) {
        int selectedItem = tblviewEvents.getSelectionModel().getSelectedIndex();

        if (selectedItem >= 0) { //проверяем выбрана ли строка таблицы перед удалением            
            TableEvent te = (TableEvent) tblviewEvents.getSelectionModel().getSelectedItem();
            try {
                deleteDB(te.getID(), te.getPeriod());
            } catch (SQLException ex) {
            }
            dataEvents.remove(selectedItem);
        } else {
            javafx.scene.Node source = (javafx.scene.Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            showPopupMessage("Вы не выбрали ни одного событий или нет ни одного события, которое можно удалить!", stage);
        }
    }

    private void deleteDB(Integer ID, String Per) throws SQLException {
        Connection connect = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            connect.setAutoCommit(false);
            stmt = connect.createStatement();
            String sql;
            if (Per.equals("Ежегодное") | Per.equals("Ежемесячное")) {
                sql = "DELETE FROM EVENTS WHERE ID = " + ID + ";";
            } else {
                sql = "DELETE FROM weekly_events WHERE ID = " + ID + ";";
            };
            stmt.executeUpdate(sql);
            connect.commit();            
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    //всплывающе окно с ошибкой
    public static Popup createPopup(final String message) {
        final Popup popup = new Popup();
        popup.setAutoFix(true);
        popup.setAutoHide(true);
        popup.setHideOnEscape(true);
        Label label = new Label(message);
        label.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                popup.hide();
            }
        });
        label.getStylesheets().add(CalendarApp.class.getResource("styleapp.css").toExternalForm());
        label.getStyleClass().add("popup");
        popup.getContent().add(label);
        return popup;
    }

    public static void showPopupMessage(final String message, final Stage stage) {
        final Popup popup = createPopup(message);
        popup.setOnShown(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent e) { //выводим в центре окна
                popup.setX(stage.getX() + stage.getWidth() / 2 - popup.getWidth() / 2);
                popup.setY(stage.getY() + stage.getHeight() / 2 - popup.getHeight() / 2);
            }
        });
        popup.show(stage);
    }

    //добавлем новые событие из строки с описанием в таблицу
    @FXML
    private void addeventAction(ActionEvent event) {
        if (tfield_addevent.getLength() > 0) {
            Integer imp = 0;
            if (checkboxImp.isSelected()) {
                imp = 1;
            }
            try {
                insertDB(tfield_addevent.getText(), imp, cbx_repeat.getSelectionModel().getSelectedIndex());
            } catch (SQLException ex) {
            }
            try {
                dataEvents.clear();
                loadEvents();
            } catch (SQLException ex) {
            }
            tfield_addevent.setText(null);
        } else {
            javafx.scene.Node source = (javafx.scene.Node) event.getSource();
            Stage stage = (Stage) source.getScene().getWindow();
            showPopupMessage("Сделайте описание события перед его добавлением!", stage);
        }
    }

    private void insertDB(String Desc, Integer Imp, Integer Per) throws SQLException {
        Connection connect = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            connect = DriverManager.getConnection("jdbc:sqlite:db/dates.db");
            connect.setAutoCommit(false);
            stmt = connect.createStatement();
            String sql;
            LocalDate date = datepicker.getValue();
            String sDay;
            if (date.toString().substring(8, 9).equals("0")) {
                sDay = date.toString().substring(9, 10);
            } else {
                sDay = date.toString().substring(8, 10);
            }
            String sMonth;
            if (date.toString().substring(5, 6).equals("0")) {
                sMonth = date.toString().substring(6, 7);
            } else {
                sMonth = date.toString().substring(5, 7);
            }            
            switch (Per) {
                case 1:
                    sql = "INSERT INTO EVENTS (DAY,MONTH,DESCRIPTION,IMPORTANT) VALUES ('"
                            + sDay + "','0','" + Desc + "','" + Imp + "');";
                    break;
                case 2:
                    sql = "INSERT INTO weekly_events (DAY_OF_WEEK,DESCRIPTION,IMPORTANT) VALUES ('"
                            + DayOfWeek() + "','" + Desc + "','" + Imp + "');";
                    break;
                default:                    
                    sql = "INSERT INTO EVENTS (DAY,MONTH,DESCRIPTION,IMPORTANT) VALUES ('"
                            + sDay + "','" + sMonth + "','" + Desc + "','" + Imp + "');";
                    break;
            }
            stmt.executeUpdate(sql);
            connect.commit();
            stmt.close();
            connect.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        } finally {
            if (connect != null) {
                connect.close();
            }
        }
    }

    private String DayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        String date = datepicker.getValue().toString();
        calendar.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)) - 1, Integer.parseInt(date.substring(8)));
        if (calendar.get(Calendar.DAY_OF_WEEK) == 1) {
            return "7";
        } else {
            return String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1);
        }
    }

    public static class TableEvent {

        private final SimpleIntegerProperty ID;
        private final SimpleStringProperty Period;
        private final SimpleStringProperty Events;
        private final SimpleStringProperty Important;

        private TableEvent(Integer sID, String sPeriod, String sEvents, String sImportant) {
            this.ID = new SimpleIntegerProperty(sID);
            this.Period = new SimpleStringProperty(sPeriod);
            this.Events = new SimpleStringProperty(sEvents);
            this.Important = new SimpleStringProperty(sImportant);
            setTextImp(sImportant);
        }

        public void setTextImp(String s) {
            if (s.equals("1")) {
                setImportant("\uf046");
            } else {
                setImportant("\uf096");
            }
        }

        public Integer getID() {
            return ID.get();
        }

        public void setID(Integer sID) {
            ID.set(sID);
        }

        public String getPeriod() {
            return Period.get();
        }

        public void setPeriod(String sPeriod) {
            Period.set(sPeriod);
        }

        public String getEvents() {
            return Events.get();
        }

        public void setEvents(String sEvents) {
            Events.set(sEvents);
        }

        public String getImportant() {
            return Important.get();
        }

        public void setImportant(String sImportant) {
            Important.set(sImportant);
        }
    }

}

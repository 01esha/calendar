package calendarapp;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class CalendarApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.initStyle(StageStyle.TRANSPARENT);
        Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
        Scene scene = new Scene(root);
        scene.setFill(null);
        stage.setScene(scene);
        Font.loadFont(CalendarApp.class.getResource("Fonts/Nord.otf").toExternalForm(), 12);
        Font.loadFont(CalendarApp.class.getResource("Fonts/Margot.ttf").toExternalForm(), 12);
        Font.loadFont(CalendarApp.class.getResource("Fonts/FontAwesome.otf").toExternalForm(), 12);
        stage.setTitle("Календарь на каждый день");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("UIelements/appicon.png")));
        moveScene(scene, stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    //перетаскивание окна программы
    public void moveScene(Scene scene, Stage stage) {
        final Delta dragDelta = new Delta();
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                dragDelta.x = stage.getX() - mouseEvent.getScreenX();
                dragDelta.y = stage.getY() - mouseEvent.getScreenY();
            }
        });
        /*
         scene.setOnMouseReleased(new EventHandler<MouseEvent>() {
         @Override public void handle(MouseEvent mouseEvent) {
         //scene.setCursor(Cursor.HAND);
         }
         });
         */
        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                stage.setX(mouseEvent.getScreenX() + dragDelta.x);
                stage.setY(mouseEvent.getScreenY() + dragDelta.y);
            }
        });
        /*
         scene.setOnMouseEntered(new EventHandler<MouseEvent>() {
         @Override public void handle(MouseEvent mouseEvent) {
         if (!mouseEvent.isPrimaryButtonDown()) {
         //scene.setCursor(Cursor.HAND);
         }
         }
         });
         scene.setOnMouseExited(new EventHandler<MouseEvent>() {
         @Override public void handle(MouseEvent mouseEvent) {
         if (!mouseEvent.isPrimaryButtonDown()) {
         //scene.setCursor(Cursor.DEFAULT);
         }
         }
         });
         */
    }

    class Delta {

        double x, y;
    }

}
